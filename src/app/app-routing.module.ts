import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterDetailsComponent } from './character-details/character-details.component';
import { CharacterListComponent } from './character-list/character-list.component';
import { Page404Component } from './error/page404/page404.component';


const routes: Routes = [

  { path: '', component: CharacterListComponent },
  { path: 'details/:id', component: CharacterDetailsComponent },
  { path: '404', component: Page404Component, pathMatch: 'full' },

  { path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
