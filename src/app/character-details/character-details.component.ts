import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ListService } from '../_services/list.service';

@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss']
})
export class CharacterDetailsComponent implements OnInit {

  charactersData = {
    "birthPlace": "",
    "dob": "",
    "id": "",
    "image": "",
    "name": "",
    "nickNames": [],
  }
  constructor(private _listService: ListService,
    private router: Router,
    private activateRoute: ActivatedRoute) {
    this.activateRoute.params.subscribe(params => {
      this._listService.getCharacterDataDetails(params.id).subscribe(res => {

        if (res.body.success) {
          this.charactersData = res.body.data
        } else {
          this.router.navigate(['404']);
        }
      }, err => { this.router.navigate(['404']); });
    })

  }


  ngOnInit() {
  }

}
