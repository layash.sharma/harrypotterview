import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  url = environment.url
  constructor(private http: HttpClient,) { }

  getAllData(): Observable<any> {
    return this.http.get(this.url, {
      observe: 'response',
    });
  }

  getCharacterDataDetails(id): Observable<any> {
    return this.http.get(`${this.url}character/${id}`, {
      observe: 'response',
    })
  }
}
