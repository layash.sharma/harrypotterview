import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ListService } from '../_services/list.service';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit {
  charactersData = [{
    "birthPlace": "",
    "dob": "",
    "id": "",
    "image": "",
    "name": "",
    "nickNames": [],
  }]
  constructor(private _listService: ListService,private router:Router) {
    this._listService.getAllData().subscribe(res => {

      if (res.body.success) {
        this.charactersData = res.body.data
      }else{
        this.router.navigate(['/404']);
      }
    }, err => { this.router.navigate(['/404']); });
  }

  ngOnInit() {
  }

}
